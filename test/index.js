var { rusToEng, image, spawnStream } = require('../index');
var { expect, assert } = require('chai');
const path = require('path');
const fs = require('fs');

it('rusToEng', () => expect('lukashenko').to.equal(rusToEng('лукашенко')));

it('image', () => {
  const obj = {
    text: 'hello world',
    man: 'lukashenko',
    color: 'blue',
    lang: 'ru',
    watermark: 't'
  };
  const img1 = image(obj);
  const img2 = [
    '-',
    '-font',
    'Arial',
    '-kerning',
    '0',
    '-background',
    'LightSteelBlue2',
    '-alpha',
    'remove',
    `${path.join(__dirname, '..', 'images')}/${obj.man}.png`,
    '-gravity',
    'west',
    '-composite',
    `${path.join(__dirname, '..', 'images')}/watermark_${obj.watermark}.png`,
    '-gravity',
    'north',
    '-composite',
    '-size',
    '420x280!',
    '-background',
    'none',
    '-fill',
    'MidnightBlue',
    '-gravity',
    'West',
    'caption:hello world',
    '-geometry',
    '+350-20',
    '-composite',
    '-size',
    '420x38!',
    '-gravity',
    'West',
    'caption:(c) Александр Лукашенко',
    '-geometry',
    '+350+160',
    '-composite',
    '-quality',
    '10',
    '-'
  ];

  return expect(img1).to.deep.equal(img2);
});

it('spawnStream', done => {
  let size = 0;

  const photo = spawnStream(
    `${path.join(__dirname, '..', 'images')}/start.png`,
    image({
      text: 'hello world',
      man: 'lukashenko',
      color: 'blue',
      lang: 'ru'
    })
  );
  photo.pipe(fs.WriteStream(__dirname + '/fff.jpg'));

  photo.on('data', function(chunk) {
    size += chunk.byteSize;
  });

  photo.on('end', function() {
    assert(size != 0);
    done();
  });
});
