module.exports = str =>
  str
    // men
    .replace(/стетхем/g, 'statham')
    .replace(/сталин/g, 'stalin')
    .replace(/гитлер/g, 'hitler')
    .replace(/дуров/g, 'durov')
    .replace(/трамп/g, 'trump')
    .replace(/норрис/g, 'norris')
    .replace(/навальный/g, 'navalny')
    .replace(/конфуций/g, 'confucius')
    .replace(/кличко/g, 'klitschko')
    .replace(/ленин/g, 'lenin')
    .replace(/порошенко/g, 'poroshenko')
    .replace(/йода/g, 'yoda')
    .replace(/серова/g, 'sashagrey')
    .replace(/моисеев/g, 'moiseev')
    .replace(/обама/g, 'obama')
    .replace(/лукашенко/g, 'lukashenko')
    .replace(/усама/g, 'osama')
    .replace(/догг/g, 'dogg')
    .replace(/ким/g, 'kim')
    // colors
    .replace(/красный/g, 'red')
    .replace(/оранжевый/g, 'orange')
    .replace(/зелёный/g, 'green')
    .replace(/зеленый/g, 'green')
    .replace(/синий/g, 'blue')
    .replace(/розовый/g, 'pink')
    .replace(/ночь/g, 'dark');
