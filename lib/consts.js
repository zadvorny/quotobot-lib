module.exports = {
  men: {
    statham: { en: 'Jason Statham', ru: 'Джейсон Стетхем', only: 'all' },
    stalin: { en: 'Joseph Stalin', ru: 'Иосиф Сталин', only: 'all' },
    hitler: { en: 'Adolf Hitler', ru: 'Адольф Гитлер', only: 'all' },
    durov: { en: 'Pavel Durov', ru: 'Павел Дуров', only: 'all' },
    norris: { en: 'Chuck Norris', ru: 'Чак Норрис', only: 'all' },
    navalny: { en: 'Aleksey Navalny', ru: 'Алексей Навальный', only: 'ru' },
    confucius: { en: 'Confucius', ru: 'Конфуций', only: 'all' },
    trump: { en: 'Donald Trump', ru: 'Дональд Трамп', only: 'all' },
    lenin: { en: 'Vladimir Lenin', ru: 'Владимир Ленин', only: 'ru' },
    klitschko: { en: 'Vitali Klitschko', ru: 'Виталий Кличко', only: 'ru' },
    poroshenko: { en: 'Petro Poroshenko', ru: 'Пётр Порошенко', only: 'ru' },
    yoda: { en: 'Master Yoda', ru: 'Мастер Йода', only: 'all' },
    sashagrey: { en: 'Sasha Grey', ru: 'Александра Серова', only: 'ru' },
    moiseev: { en: 'Boris Moiseev', ru: 'Борис Моисеев', only: 'ru' },
    obama: { en: 'Barack Obama', ru: 'Барак Обама', only: 'all' },
    lukashenko: {
      en: 'Alexander Lukashenko',
      ru: 'Александр Лукашенко',
      only: 'ru'
    },
    osama: { en: 'Osama bin Laden', ru: 'Усама бен Ладен', only: 'all' },
    dogg: { en: 'Snoop Dogg', ru: 'Снуп Догг', only: 'en' },
    kim: { en: 'Kim Jong Un', ru: 'Ким Чен Ын', only: 'all' }
  },
  colors: {
    grey: {
      text: 'grey3',
      bg: 'grey',
      name: {
        ru: 'Серый',
        en: 'Grey'
      }
    },
    red: {
      text: 'DarkRed',
      bg: 'RosyBrown3',
      name: {
        ru: 'Красный',
        en: 'Red'
      }
    },
    orange: {
      text: 'DarkOrange4',
      bg: 'orange',
      name: {
        ru: 'Оранжевый',
        en: 'Orange'
      }
    },
    green: {
      text: 'DarkGreen',
      bg: 'DarkSeaGreen3',
      name: {
        ru: 'Зелёный',
        en: 'Green'
      }
    },
    blue: {
      text: 'MidnightBlue',
      bg: 'LightSteelBlue2',
      name: {
        ru: 'Синий',
        en: 'Blue'
      }
    },
    pink: {
      text: 'maroon4',
      bg: 'HotPink',
      name: {
        ru: 'Розовый',
        en: 'Pink'
      }
    },
    dark: {
      text: 'WhiteSmoke',
      bg: 'gray7',
      name: {
        ru: 'Чёрный',
        en: 'Black'
      }
    }
  }
};
