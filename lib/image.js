const { men, colors } = require('./consts');
const path = require('path');
const isProd = process.env.NODE_ENV === 'production';

module.exports = ({ text, man, color, lang, watermark }) => {
  const manName = men[man][lang];
  const textColor = colors[color].text;
  const bgColor = colors[color].bg;
  const watermarkFile = watermark || 'vk';

  return [
    '-', // use stdin
    '-font',
    isProd ? 'DejaVu-Sans' : 'Arial',
    '-kerning',
    isProd ? '-2' : '0',
    '-background',
    bgColor,
    '-alpha',
    'remove',
    ///
    `${path.join(__dirname, '..', 'images')}/${man}.png`,
    '-gravity',
    'west',
    '-composite',
    // ///
    `${path.join(__dirname, '..', 'images')}/watermark_${watermarkFile}.png`,
    '-gravity',
    'north',
    '-composite',
    ///
    '-size',
    '420x280!',
    '-background',
    'none',
    '-fill',
    textColor,
    '-gravity',
    'West',
    'caption:' + text,
    '-geometry',
    '+350-20',
    '-composite',
    ///
    '-size',
    '420x38!',
    '-gravity',
    'West',
    'caption:(c) ' + manName,
    '-geometry',
    '+350+160',
    '-composite',
    ///
    '-quality',
    '10',
    '-' // output to stdout
  ];
};
