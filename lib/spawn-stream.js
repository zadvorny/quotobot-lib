const { spawn } = require('child_process');
const Duplexify = require('duplexify');
const { isError } = require('util');
const fs = require('fs');

class SuperSpawn extends Duplexify {
  constructor(path, args) {
    super();
    this.args = args;
    this.spawned = false;

    if (path) {
      const file = fs.createReadStream(path);
      file.on('error', this.onerror);
      file.pipe(this);
    }
  }

  resume() {
    if (!this.spawned) this.spawn();
    this.spawned = true;
    super.resume();
  }

  spawn() {
    const onerror = this.onerror.bind(this);
    const proc = spawn('convert', this.args);

    const stdout = proc.stdout;
    stdout.on('error', onerror);
    this.setReadable(stdout);

    const stdin = proc.stdin;
    stdin.on('error', onerror);
    this.setWritable(stdin);

    const stderr = proc.stderr;
    stderr.on('data', onerror);
    stderr.on('error', onerror);
  }

  onerror(err) {
    // if (!isError(err)) err = new Error(err);
    // if (!this.listeners('error')) throw err;
    // this.emit('error', err);
  }
}

module.exports = (path, args) => new SuperSpawn(path, args);
