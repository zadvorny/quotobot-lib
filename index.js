const spawnStream = require('./lib/spawn-stream');
const image = require('./lib/image');
const rusToEng = require('./lib/rusToEng');
const imgConsts = require('./lib/consts');

module.exports = {
  spawnStream,
  image,
  rusToEng,
  imgConsts
};
